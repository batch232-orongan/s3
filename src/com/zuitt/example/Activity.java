package com.zuitt.example;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Activity {
    public static void main(String[] args){
        HashMap<String, Integer> games = new HashMap<>();
        games.put("Mario Odyssey", 50);
        games.put("Super Smash Bros. Ultimate", 20);
        games.put("Luigi's Mansion 3", 15);
        games.put("Pokemon Sword", 30);
        games.put("Pokemon Shield", 100);

        games.forEach((key, value) -> {
            System.out.println(key + " has "+ value + " stocks left.");
        });

        ArrayList<String> topGames = new ArrayList<>();

        games.forEach((key, value) -> {
            if( value <= 30){
                topGames.add(key);
                System.out.println(key + " has been addded to the top games list!");
            }
        });
        System.out.println("Our shop's top games: ");
        System.out.println(topGames);

        // Stretch Goals
        Scanner usersInput = new Scanner(System.in);
        boolean addItem = true;

        while(addItem){
            System.out.println("Would you like to add an item? Yes or No?");
            String input = usersInput.nextLine();

            if(input.equalsIgnoreCase("Yes")){

                System.out.println("Add the item name");
                String itemName = usersInput.nextLine();

                System.out.println("Add the item stock");
                int itemStock = usersInput.nextInt();

                System.out.println(itemName + " has been added with " + itemStock + " stocks");

//                HashMap<String, Integer> newItem = new HashMap<>();
//                newItem.put(itemName, itemStock);
                games.put(itemName, itemStock);
                System.out.println(games);
                addItem = false;
                break;
            } else if (input.equalsIgnoreCase("No")) {
                addItem = false;
                System.out.println("Thank you!");
            } else {
                System.out.println("Input is Invalid!");
            }
        }
    }
}
