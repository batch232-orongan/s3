package com.zuitt.example;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;
import java.util.function.BiConsumer;

public class Loops {
    public static void main(String[] args){
        // Loops are control structure that allow code blocks to be repeated according to the conditions set.
        //Types of Loop
            // While Loop - allows us to repeat action / code block if it meets the condition
        int a = 1;

        while(a < 5){
            System.out.println("While loop counter: "+ a);
            a++;
        }
        Scanner userInput = new Scanner(System.in);
        boolean hasNoInput = true;
        while(hasNoInput){
            System.out.println("Enter your name:");
            String name = userInput.nextLine();

            if(name.isEmpty()){
                System.out.println("Please try again:");
            } else {
                hasNoInput = false;
                System.out.println("Thank for your input");
            }
        }
            // do-while Loop - will run at least once
        int b=5;

        do{
            System.out.println("Countdown: " + b);
            b--;
        }while (b >= 1);

            // for Loop
        for(int i=0; i <= 10; i++){
            System.out.println("Count: " + i);
        }

            // for loop over a Java Array

        int [] intArray = {100, 200, 300, 400, 500};
        for(int i = 0; i < intArray.length; i++ ){
            System.out.println("Item at index number " + i + " is " + intArray[i]);
        }
            // Multidimensional array
            // first array could be for the rows, second array is for columns.
        String [][] classroom = new String[3][3];

                // first row
        classroom [0][0] = "Tony";
        classroom [0][1] = "Steve";
        classroom [0][2] = "Bruce";

                // second row
        classroom [1][0] = "Natasha";
        classroom [1][1] = "Wanda";
        classroom [1][2] = "Clint";

                // third row
        classroom [2][0] = "Scott";
        classroom [2][1] = "Peter";
        classroom [2][2] = "Stephen";

        // nested for loops
        // the inner loop executes completely whenever outer loops executes
        for(int row=0; row < 3; row++){
            for(int col=0; col < 3; col++){
                System.out.println(classroom[row][col]);
            }
        }
        // deepToString() is used to display the values of a multidimensional array
        System.out.println(Arrays.deepToString(classroom));

            // enhanced for loop for arrays and array lists
            // In Java, we can use an enhanced for loop to loop over EACH item in an array or arraylist.
            // forEach in Java
                String[] members = {"Eugene", "Vincent", "Dennis", "Alfred"};
                // member is a parameter representing an item in the members array, even as a parameter, you have to indicate the data type.
                for(String member: members){
                    System.out.println(member);
                }
                // enhanced for loop in multidimensional array
                for(String[] row: classroom){
                    // row -  each array
                    for(String student: row){
                        System.out.println(student);
                    }
                }

            // for each for hashmap
        HashMap<String, String> techniques = new HashMap<>();
        techniques.put(members[0], "Spirit Gun" );
        techniques.put(members[1], "Black Dragon" );
        techniques.put(members[2], "Rose Whip" );
        techniques.put(members[3], "Spirit Sword" );
        System.out.println(techniques);
        // hashmap forEach() requires a lambda expression as an argument
        // Lambda expression, short block of code which takes in parameters and returns a value
        // Lambda expression are similar to methods, but they do not have a name and are implemented within another method

        techniques.forEach((key, value) -> {
            System.out.println("Member " + key + " uses " + value);
        });
    }
}
